#### AWS custom utility to upload files and get download urls: Ruby code([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/aws_samples))

    AWS sample code. 

#### Find Minimum number of clicks: Ruby code([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/ruby_remote_control_problem))

	Interesting remote control problem solved using Ruby Script.

#### Trip Planner Application ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/trip_planner))


    A web application to find one-way/return/multi-city trips from a list of given train schedules. 
    Live application link: http://train-trip-planner.herokuapp.com/

#### BrowserStack API Wrapper ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/bs_wrapper))

    Wrapper around BrowserStack API

#### Custom Signup: Basic CRUD Application ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/custom_signup))

    Custom Signup and Category CRUD 
    Live Application: https://custom-signup-app.herokuapp.com 

#### Hotel booking problem ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/hotel_booking_problem))


    Hotel room booking solution in Ruby.

#### Basic ecommerce platform built with Spree ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/your_own_store))


    Basic e-commerce platform application built with Spree.


#### Push notification application for android and ios ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/push_notification))


    Push notification application for android and ios.
    
#### Sample Angular Js aplication ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/Angular_app))


    Sample Angular Js aplication.

#### Expense Tracking Appication ([Click here](https://github.com/idyllicsoftware/public_sample_code/tree/master/expense-track-assignment))


    Expense Tracking Appication.
