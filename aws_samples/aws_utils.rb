require 'fileutils'
class Util::AwsUtils

 # Method to initizlise aws object.
 # Date:: 24/03/2015
  #
  # <b>Returns</b>
  # * s3_object <em>(Aws::S3::Client)</em> s3 client 
  def self.get_aws_object
    s3 = Aws::S3::Client.new(
      :access_key_id => AWS_CONFIG['aws_access_key_id'],
      :secret_access_key => AWS_CONFIG['aws_secret_accerss_key'],
      :region => AWS_CONFIG['region']
      )
    return s3
  end

  # a general utility to upload file to s3
  # this method do not uses temporary storage
  # Date:: 24/03/2015
  #
  # <b>Expects</b>
  # * <b>path</b><em>(String)</em> Path of the file to upload
  # * <b>bucket</b><em>(String)</em> Bucket name for file to upload
  # * <b>s3_location</b><em>(String)</em> S3 location for file to upload
  # * <b>options</b><em>(Hash)</em> Hash of options
  # * <b>options example</b>(Hash)</em> options = {acl: 'public-read | authenticated-read .. '}
  #
  # <b>Returns</b>
  # * <b>status response </b> <em>(Has)</em> status of the upload
  # * <b>status response example</b>(Hash)</em> {status: 'error|success', msg: 'Message'}
  def self.direct_upload(path, bucket, s3_location, options = {})
    begin
      # upload file to s3
      acl_flag = options[:acl] || 'authenticated-read'
      content_type = get_content_type(options[:original_filename])
      s3 = get_aws_object
      File.open(path, 'rb') do |file|
        resp = s3.put_object(
          :bucket => bucket,
          :key => s3_location,
          :body => file,
          :acl => acl_flag,
          :content_type => content_type
          )
      end
    rescue Aws::S3::Errors::ServiceError => exc
      return {status: 'error', msg: exc.message}
    end
    return {status: 'success', msg: ''}
  end

  # Method to get content based on file name
  # Content type is necessary to mange file properly while downloading. 
  # Date:: 24/03/2015
  #
  # <b>Expects</b>
  # * <b>original_filename</b><em>(String)</em> file name 
  #
  # <b>Returns</b>
  # * content_type <em>(String)</em> content type for the file
  def self.get_content_type(original_filename)
    content_types = {
      'csv' => 'text/csv',
      'doc' => 'application/msword',
      'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'xls' => 'application/vnd.ms-excel',
      'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'ppt' => 'application/vnd.ms-powerpoint',
      'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    }
    file_extension = original_filename.split('.').last.downcase rescue ''
    content_types[file_extension]
  end

  # a general utility methods to upload file to s3
  # note this method is speciallly designed to upload input via form tags
  # this method uses temparary storage
  # Date:: 24/03/2015
  #
  # <b>Expects</b>
  # * <b>file_to_upload</b><em>(File Object)</em> File to Upload
  #
  # <b>Returns</b>
  # * status <em>(Boolean)</em> upload status
  def self.upload_using_tmp(input_file, bucket, s3_location, options = {})

    name =  input_file.original_filename
    #Create local directory for uploads if not exists
    unless File.directory?(Attachment::TMP_UPLOAD_FOLDER)
      FileUtils.mkdir_p(Attachment::TMP_UPLOAD_FOLDER)
    end

    directory = Attachment::TMP_UPLOAD_FOLDER
    # create the file path
    path = File.join(directory, name)

    begin
      # write the file to local path
      File.open(path, "wb") { |f| f.write(input_file.read) }
      direct_upload(path, bucket, s3_location, options)
    rescue Exception => e
      Airbrake.notify(e)
      return {status: 'error', msg: e.message}
    end

    File.delete(path) rescue ''
    return {status: 'success', msg: ''}
  end

  # Method which freturns encoded url to the s3 uploadec content
  # Date:: 24/03/2015
  #
  # <b>Expects</b>
  # * <b>s3_file_name</b><em>(String)</em> File name
  #
  # <b>Returns</b>
  # * s3_url <em>(String)</em> complete URI
  def self.get_s3_url(s3_file_name)
    url = "https://#{AWS_CONFIG['bucket']}.s3.amazonaws.com/#{s3_file_name}"
    return URI.encode(url)
  end

  # Method to copy content from one s3 location to other s3 location
  # Date:: 24/03/2015
  #
  # <b>Expects</b>
  # * <b>from_location</b><em>(String)</em> s3 file to copy
  # * <b>to_location</b><em>(String)</em> s3 file copy location
  #
  # <b>Returns</b>
  # * s3_url <em>(String)</em> complete URI
  def self.copy_file(from_location, to_location)
    error = ''
    begin
      bucket_name = AWS_CONFIG['bucket']
      s3 = get_aws_object
      s3.copy_object(bucket:bucket_name, key:to_location, copy_source:"#{bucket_name}/#{from_location}", acl:'public-read')
    rescue  Exception => e
      Airbrake.notify(e)
      Rails.logger.debug("EXCEPTION in Copy File: Message: #{e.message}\n\n Backtrace: #{e.backtrace}")
      error << e.message
    end
    if error.blank?
      return {status: 'success', msg: ''}
    else
      return {status: 'error', msg: error}
    end
  end

end
